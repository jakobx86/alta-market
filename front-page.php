<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alta-market-new
 */

get_header();
?>	
					<div id="primary" class="content-area ">						
						<main id="main" class="site-main">
              <?php
                                $args = array(
                                'taxonomy' => 'product_cat',
                                'orderby'    => 'count',
                                'order'      => 'DESC',
                                'hide_empty' => false
                            );

                            $product_categories = get_terms( $args );

                            //get_vd($product_categories);

                            $count = count($product_categories);                          
                          
                          ?>

<section class="categories__section" id="categories__section">
            <div class="categories__list">
        <div class="container">
          <div class="row">
            <?php if ( $count > 0 ):
                                foreach ( $product_categories as $product_category ) : 
                                  if ($product_category->parent ==0 && $product_category->slug!='uncategorized') :
                                  $category_thumbnail = get_term_meta($product_category->term_id, 'thumbnail_id', true);
    $image = wp_get_attachment_url($category_thumbnail,'thumbnail');?>    
                          <div class="col-6 col-md-4 col-lg-3">

                <a href="<?php echo get_category_link( $product_category->term_id )?>" class="categories__item">
                  <div class="categories__item-img" style="
                      background-position-x: 170px;   
     
  background: url('<?php echo  $image ? $image : get_template_directory_uri() . '/assets/image/lion.jpg';?>') no-repeat 140px;   ">
                    <!-- <img alt="#"  src="<?php echo  $image ? $image : get_template_directory_uri() . '/assets/image/lion.jpg' ?>"> -->
                    <span class="categories__item-label"><?php echo $product_category->name ?></span>
                  </div>
                  
                </a>
              </div>
  <?php endif; ?>        
<?php endforeach ; ?>
<?php endif; ?>
</div>
        </div>
      </div>
    </section>
 <section>
  <div class="container">
  <div class="row separator">
 <div class="col-md-12"><br>
  <p class="title_section">Хиты</p></div>
</div>
  <?php echo do_shortcode( '[best_selling_products per_page="4" columns="4"]' ); ?>
</div>
 </section>
 <section>
  <div class="container">
  <div class="row separator">
 <div class="col-md-12"><br>
  <p class="title_section">Новинки</p></div>
</div>
  <?php echo do_shortcode( '[recent_products per_page="4" columns="4"]' ); ?>
</div>
 </section>
  <section class="section">
    <div class="container">
      <div class="home__description">
						<h1>Front page</h1>				
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
          </div>
          </div>
          </section>			
		</main><!-- #main -->
	</div><!-- #primary -->
				

<?php

get_footer();
