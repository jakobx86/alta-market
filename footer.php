<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alta-market-new
 */

?>
<svg style="display: none;">
  <symbol version="1.1" id="icon_mail"  x="0px" y="0px" viewBox="-8 -8 35 35">

        <path fill="#FFFFFF" d="M9.5,14.2l-4.1-3.6l-11.6,10c0.4,0.4,1,0.6,1.6,0.6h28.1c0.6,0,1.2-0.2,1.6-0.6l-11.6-10L9.5,14.2z"/>
        <path fill="#FFFFFF" d="M25.2-1.6c-0.4-0.4-1-0.6-1.6-0.6H-4.6c-0.6,0-1.2,0.2-1.6,0.6L9.5,11.8L25.2-1.6z"/>
        <polygon fill="#FFFFFF" points="-6.9,-0.2 -6.9,19.3 4.4,9.7         "/>
        <polygon fill="#FFFFFF" points="14.6,9.7 25.9,19.3 25.9,-0.2        "/>
</symbol><symbol width="30" height="30" version="1.1" id="icon_mark_footer" x="0px" y="0px" viewBox="195.5 195.5 35 35"><path fill="#FFFFFF" d="M213,196.1L213,196.1c-5.9,0-10.6,4.7-10.6,10.6c0,3.8,1.7,8.9,5.2,15c2.5,4.5,5.1,8,5.1,8c0.1,0.1,0.2,0.2,0.3,0.2c0,0,0,0,0,0c0.1,0,0.2-0.1,0.3-0.2c0,0,2.6-3.9,5.1-8.6c3.4-6.4,5.1-11.2,5.1-14.4
C223.6,200.9,218.8,196.1,213,196.1zM217.9,206.9c0,2.7-2.2,4.9-4.9,4.9c-2.7,0-4.9-2.2-4.9-4.9s2.2-4.9,4.9-4.9C215.7,202,217.9,204.2,217.9,206.9z"/></symbol><symbol version="1.1" id="icon_phone_footer" x="0px" y="0px" viewBox="219.5 219.5 20 20"><path fill="#0D449C" d="M224,228.3c1.4,2.9,3.9,5.2,6.7,6.7l2.2-2.2c0.3-0.3,0.7-0.4,1-0.2c1.1,0.4,2.3,0.6,3.7,0.6
c0.6,0,1,0.4,1,1v3.5c0,0.6-0.4,1-1,1c-9.6,0-17.3-7.8-17.3-17.3c0-0.6,0.4-1,1-1h3.6c0.6,0,1,0.4,1,1c0,1.2,0.2,2.4,0.6,3.7c0.1,0.3,0,0.7-0.2,1L224,228.3z"/></symbol>
<symbol  width="65" height="24" id="icon_arrow" x="-20px" y="-5px" viewBox="0 0 20 20">
<path fill="#0D449C" d="M22.4,15.7c0,0.6-0.2,1.3-0.7,1.8c-1,1-2.6,1.1-3.6,0.1l-6-5.7l-6,5.7c-1,1-2.6,0.9-3.6-0.1
          c-1-1-0.9-2.6,0.1-3.6l7.8-7.4c1-0.9,2.5-0.9,3.5,0l7.8,7.4C22.1,14.4,22.4,15,22.4,15.7z"/></symbol>
</svg>

  </div><!-- #content -->
  </div>
  <footer class="footer">
<div class="btn--scroll js-scrollTop d-flex" data-scroll="content">
<svg>
  <use xlink:href="#icon_arrow"></use>
</svg>
</div>
<div class="container">
  <div class="row">
    <?php
    if (is_active_sidebar( 'sidebar-footer' ) )
    {
      dynamic_sidebar('sidebar-footer' );
    }    
     ?>   
<div class="col-md-4  offset-md-2">
  <div class="footer_wraper_contacts">
    <div class="mark_icon">
      <svg>
        <use xlink:href="#icon_mark_footer"></use>
      </svg>
    </div>
    <?php 
    $tel_first= carbon_get_theme_option( 'af_first_phone' );
    $tel_second= carbon_get_theme_option( 'af_second_phone' );
    $mail= carbon_get_theme_option( 'af_email' );
    $copy_right= carbon_get_theme_option( 'af_copyright' );
     ?>
    <p class="contact_text">г. Киев, ул. Кириловская 40</p>
  </div>
  <div class="footer_wraper_contacts">
    <div class="phone_icon">
      <svg>
        <use xlink:href="#icon_phone_footer"></use>
      </svg>
    </div>
    <p class="contact_text"><?php echo $tel_first ? $tel_first : "+38 (067)  446 12 18"?> </p>
  </div>
  <div class="footer_wraper_contacts">
    <div class="phone_icon">
      <svg>
        <use xlink:href="#icon_phone_footer"></use>
      </svg>
    </div>
    <p class="contact_text"><?php echo $tel_second ? $tel_second : "+38 (050)  215 99 37"?> </p>
  </div>
  <div class="footer_wraper_contacts">
    <div class="mail_icon">
      <svg>
        <use xlink:href="#icon_mail"></use>
      </svg>
    </div>
    <p class="contact_text"><?php echo  $mail ? $mail : "alex3245@gmail.com" ?></p>
  </div>     
    </div>
  </div>
</div>
</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
