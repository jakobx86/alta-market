<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function alta_market_new_scripts() {
	

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array(), '20151215', true );
	wp_enqueue_script( 'main_js', get_template_directory_uri() . '/assets/js/main.min.js', array(), '20151215', true );

	wp_enqueue_script( 'alta-market-new-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'ajax-comments', get_template_directory_uri() . '/assets/js/ajax_comments.js', array(), '20151215', true );
	wp_localize_script( 'ajax-comments', 'comments',array('url'=>admin_url( 'admin-ajax.php'),'nonce'=>wp_create_nonce('comments_nonce' ))) ;

	wp_enqueue_script( 'alta-market-new-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'alta_market_new_scripts' );

function alta_market_new_styles() {
	wp_enqueue_style( 'alta-market-new-style', get_stylesheet_uri() );
	
}
add_action( 'wp_enqueue_scripts', 'alta_market_new_styles' );