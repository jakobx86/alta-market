<?php

if(!defined('ABSPATH'))
{
	exit();//Exit if acess directly to file
}
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

use Carbon_Fields\Container;
use Carbon_Fields\Field;

$slides = carbon_get_post_meta( get_the_ID(), 'crb_slides' );
if ( $slides ) {
    foreach ( $slides as $slide ) {
        echo $slide['image'];
    }
}
