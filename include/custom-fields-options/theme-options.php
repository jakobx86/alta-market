<?php

if(!defined('ABSPATH'))
{
	exit();//Exit if acess directly to file
}
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

use Carbon_Fields\Container;
use Carbon_Fields\Field;



Container::make( 'theme_options', __( 'Настройки темы' ) )
    ->set_icon( 'dashicons-carrot' )  
    ->add_tab( __( 'Шапка' ), array(
        Field::make( 'image', 'ah_logo', __( 'Логотип' ) ),    
        Field::make( 'text', 'ah_first_phone', __( 'Номер телефона 1' ) ),
        Field::make( 'text', 'ah_second_phone', __( 'Номер телефона 2' ) ),
        Field::make( 'text', 'ah_email', __( 'Почтовый ящик' ) ),
    ) )
    ->add_tab( __( 'Подвал' ), array(          
        Field::make( 'text', 'af_first_phone', __( 'Номер телефона 1' ) ),
        Field::make( 'text', 'af_second_phone', __( 'Номер телефона 2' ) ), 
        Field::make( 'text', 'af_copyright', __( 'Копирайт' ) ),       
        Field::make( 'text', 'af_email', __( 'Почтовый ящик' ) ),

    ) );



