<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

register_nav_menus(array('Primary'=>'Основное', 'Secondary'=>'Вторичное'));

function alta_market_primary_menu()
{
	wp_nav_menu(array( 'theme_location'=>'Primary',
	'container'       => false,		
	'menu_class'      => 'menu js-menu', 
	'walker' => new My_Walker_Nav_Menu()
	 ));
}
class My_Walker_Nav_Menu extends Walker_Nav_Menu {	
  function start_lvl( &$output, $item, $depth = 0, $args = array(), $id = 0) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul class=\"menu-sub\">\n";
  }
}

add_filter( 'widget_nav_menu_args', 'alta_add_classes_widget_footer_ul', 10);
function alta_add_classes_widget_footer_ul($nav_menu_args)
{
	$nav_menu_args['theme_location']='Secondary';
	$nav_menu_args['menu_class']='bottom_menu_list';
	$nav_menu_args['menu']='bottom';
	$nav_menu_args['container']='false';
	return $nav_menu_args;
}
add_filter( 'nav_menu_css_class', 'alta_add_classes_widget_footer_li', 10, 4 );
function alta_add_classes_widget_footer_li( $classes, $item, $args, $depth ){
	if(  $args->theme_location === 'Secondary' ){
		$classes[] = 'bottom_menu_item';
	}

	return $classes;

	// return $classes;
}
?>