<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function alta_market_new_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'alta-market-new' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'alta-market-new' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s filter_products_block sort">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title filter_products_title sort">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => "Левый сайдбар",
		'id'            => 'sidebar-footer',
		'description'   => esc_html__( 'Add widgets here.', 'alta-market-new' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s col-md-3 offset-md-1 bottom_menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title menu_title">',
		'after_title'   => '</p>',
	) );
	
}
add_action( 'widgets_init', 'alta_market_new_widgets_init' );

