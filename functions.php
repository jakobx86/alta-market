<?php
/**
 * alta-market-new functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package alta-market-new
 */

add_action( 'after_setup_theme', 'crb_load');

function crb_load()
{
	load_template( get_template_directory().'/include/carbon-fields/vendor/autoload.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}

add_action( 'carbon_fields_register_fields', 'ast_register_carbonfields' );

function ast_register_carbonfields ()
{
	require get_template_directory() . '/include/custom-fields-options/meta-box.php';
require get_template_directory() . '/include/custom-fields-options/theme-options.php';
}





/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

require get_template_directory() . '/include/widget_areas.php';
/**
 * Enqueue scripts and styles.
 */

require get_template_directory() . '/include/unique_scripts.php';
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/include/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/include/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/include/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/include/customizer.php';

require get_template_directory() . '/include/helper.php';


/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/include/jetpack.php';
}
require get_template_directory() . '/include/navigations.php';
/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/include/woocommerce.php';
	require get_template_directory() . '/woocommerce/include/wc-functions.php';
	require get_template_directory() . '/woocommerce/include/wc-functions-cart.php';
	require get_template_directory() . '/woocommerce/include/wc-functions-remove.php';
	require get_template_directory() . '/woocommerce/include/wc-functions-currency.php';
	require get_template_directory() . '/woocommerce/include/wc-functions-rating.php';
	require get_template_directory() . '/woocommerce/include/wc-functions-desription.php';
}
