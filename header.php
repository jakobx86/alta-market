<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alta-market-new
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<svg  style="display: none;">	
	<symbol version="1.1" id="icon_heart" x="0px" y="0px" viewBox="-1.5 -1.5 22 22"><path d="M9.5,18.8c-0.3,0-0.6-0.1-0.8-0.3c-0.8-0.7-1.7-1.4-2.4-2l0,0c-2.1-1.8-3.9-3.3-5.2-4.9c-1.4-1.7-2.1-3.3-2.1-5.1c0-1.7,0.6-3.3,1.7-4.5c1.1-1.2,2.6-1.8,4.2-1.8c1.2,0,2.3,0.4,3.3,1.1c0.5,0.4,0.9,0.8,1.3,1.4c0.4-0.5,0.8-1,1.3-1.4c1-0.8,2.1-1.1,3.3-1.1c1.6,0,3.1,0.6,4.2,1.8c1.1,1.2,1.7,2.7,1.7,4.5c0,1.8-0.7,3.4-2.1,5.1c-1.3,1.5-3.1,3.1-5.2,4.9c-0.7,0.6-1.5,1.3-2.4,2C10.1,18.7,9.8,18.8,9.5,18.8z M4.9,1.4c-1.3,0-2.4,0.5-3.3,1.4C0.7,3.8,0.3,5.1,0.3,6.5c0,1.5,0.6,2.8,1.8,4.3c1.2,1.4,3,3,5,4.7l0,0c0.7,0.6,1.5,1.3,2.4,2.1c0.9-0.7,1.7-1.4,2.4-2.1c2.1-1.8,3.8-3.3,5-4.7c1.2-1.5,1.8-2.8,1.8-4.3c0-1.4-0.5-2.7-1.3-3.6c-0.8-0.9-2-1.4-3.3-1.4c-0.9,0-1.8,0.3-2.5,0.9c-0.7,0.5-1.1,1.2-1.4,1.6C10,4.2,9.8,4.3,9.5,4.3S9,4.2,8.8,3.9C8.6,3.5,8.1,2.8,7.4,2.3C6.7,1.7,5.8,1.4,4.9,1.4z"/></symbol>
	<symbol version="1.1" id="icon_user"  x="0px" y="0px" viewBox="-2 -2 23 23"><path d="M9.5,10.8c3.2,0,5.8-2.6,5.8-5.8s-2.6-5.8-5.8-5.8S3.6,1.7,3.6,4.9S6.2,10.8,9.5,10.8z M9.5,0.6c2.4,0,4.3,1.9,4.3,4.3s-1.9,4.3-4.3,4.3S5.2,7.3,5.2,4.9S7.1,0.6,9.5,0.6z"/><path d="M-0.6,19.9h20.3c0.4,0,0.8-0.3,0.8-0.8c0-4-3.3-7.3-7.3-7.3H5.9c-4,0-7.3,3.3-7.3,7.3C-1.4,19.6-1.1,19.9-0.6,19.9zM5.9,13.3h7.2c2.9,0,5.4,2.2,5.7,5H0.2C0.6,15.5,3,13.3,5.9,13.3z"/></symbol>
</svg>
<body <?php body_class(); ?>>
<div id="page" class="site ">	
<div class="fixcontent">
	<header id="masthead" class="header js-header white site-header">
		<div class="container mobile_menu">
      <div class="row align-items-center justify-content-between sm-no-gutters">
        <div class="col-4 col-lg-3">
              <span class="header__logo"> 
              <?php $logo_id=  carbon_get_theme_option( 'ah_logo' );
                $logo_img= $logo_id ? wp_get_attachment_image_src( $logo_id,'full') : '';   
              // get_pr($logo_img);            
              ?>               
                  <a href="<?php echo home_url(0);?>"><img alt="logo" class="d-none d-sm-block" src="<?php echo $logo_img ? $logo_img[0] : get_template_directory_uri().'/assets/image/logo.png'?>"></a>
                </span>
                    </div>
        <div class="col-lg-4">          
<div class="search">
  <form action="<?php echo esc_url( home_url( '/' ) );?>" method="get" id="search" class="search-form js-search-form">
    <label for="search"></label>
    <input autocomplete="off"  type="text" name="s" value="<?php get_search_query();?>" class="js-header-search" id="search-input">
    <div class="search__icon js-search-icon">
      <svg>
        <use xlink:href="http://new-alta-market.1gb.ua/wp-content/themes/alta-market-new/assets/icons/sprite_symbol.svg#icon_search"></use>
      </svg>
    </div>
  </form>
  <div class="search__popup-overlay js-search-popup-overlay"></div>
  <div class="search__popup">
    <img class="search__preloader js-search-preloader" alt="preloader" src="">
    <div class="js-search-popup">
      
    </div>
  </div>
</div>
</div>         
        
        <div class="address_header_wraper col-lg-5">
          <div class="adress_header ">
            <div class="mark_icon icon_header">
      <svg>
        <use xlink:href="http://new-alta-market.1gb.ua/wp-content/themes/alta-market-new/assets/icons/sprite_symbol.svg#icon_mark"></use>
      </svg>
    </div>
            <span class="blue_text address_text">
              Офис: г. Киев, <br>
              ул. Кириловская 40
            </span>
          </div>
          <div class="phone_header blue_text">
            <div class="phone_icon icon_header">
              <svg>
        <use xlink:href="http://new-alta-market.1gb.ua/wp-content/themes/alta-market-new/assets/icons/sprite_symbol.svg#icon_phone"></use>
      </svg>
            </div>
            <?php 
              $tel1=carbon_get_theme_option( 'ah_first_phone' );
              $tel2=carbon_get_theme_option( 'ah_second_phone' );
             ?>
            <div class="phone_wraper">
              <a href="<?php echo "tel:". $tel1 ? $tel1 : '+380442580258'?>" class="phone1"><?php echo $tel1 ? $tel1 : '+380442580258'?></a> <br>
            <a  href="<?php echo "tel:" . $tel2 ? $tel2 : '+380442580258'?>" class="phone2"><?php echo $tel2 ? $tel2 : '+380442580258'?></a>
            </div>            
          </div>
        </div>
          
        
      </div>
    </div>
    <div class="container">
      <div class="row">
      <div class="col-md-6 col-12 offset-md-3 header__nav">
         <ul class="menu__tabs d-lg-none">
              <li class="menu__tabs-item active">Категории</li>
              <li class="menu__tabs-item">Меню</li>
            </ul>
            <div class="menu__wrapper">
              <ul class="menu--cats js-menu d-lg-none">
                                                       <li>
                          <a href="akcii">Шкафы витрины и торговые прилавки</a>
                        </li>
                                              <li>
                          <a href="manekeny">Стойки для товаров</a>
                        </li>
                                              <li>
                          <a href="plechiki">Торгово Выстовочные стенды</a>
                        </li>
                                              <li>
                          <a href="truby-hromirovannye">Торговое уборудывание для одежды и обуви</a>
                        </li>
                                              <li>
                          <a href="veshalki-i-stoyki">Плстиковые подставки</a>
                        </li>
                                              <li>
                          <a href="ekonompaneli-i-komplektuyushchie">Тележки и корзины</a>
                        </li>
                                              <li>
                          <a href="profil-perforirovannyy">Мнекены</a>
                        </li>
                                              <li>
                          <a href="setka-torgovaya">Стойки для одежды</a>
                        </li>
                                              <li>
                          <a href="kronshteyny">Выставочные стенды</a>
                        </li>
                                              <li>
                          <a href="kryuchki">Доп оборудывние</a>
                        </li>
                                              <li>
                          <a href="nastennye-sistemy">Сетка хромированная</a>
                        </li>
                                              <li>
                          <a href="trosovye-sistemy">Рекламные конструкции</a>
                        </li>
                                              <li>
                          <a href="fermy-hromirovannye">Оборудывание для косметики</a>
                        </li>
                                              <li>
                          <a href="zerkala-napolnye">Эксклюзивные изделия</a>
                        </li>
                                                </ul>
              <?php alta_market_primary_menu(); ?>                                  
             <!--  <ul class="menu js-menu">
                                  <li class="d-none d-md-block">
                    <a href="#">Каталог товаров<span class="menu-sub__toggle"><svg><use xlink:href="http://new-alta-market.1gb.ua/wp-content/themes/alta-market-new/assets/icons/sprite_symbol.svg#icon_triangle_down"></use></svg></span></a>
                    <ul class="menu-sub">
                                              <li>
                          <a href="akcii">Шкафы витрины и торговые прилавки</a>
                        </li>
                                              <li>
                          <a href="manekeny">Стойки для товаров</a>
                        </li>
                                              <li>
                          <a href="plechiki">Торгово Выстовочные стенды</a>
                        </li>
                                              <li>
                          <a href="truby-hromirovannye">Торговое уборудывание для одежды и обуви</a>
                        </li>
                                              <li>
                          <a href="veshalki-i-stoyki">Плстиковые подставки</a>
                        </li>
                                              <li>
                          <a href="ekonompaneli-i-komplektuyushchie">Тележки и корзины</a>
                        </li>
                                              <li>
                          <a href="profil-perforirovannyy">Мнекены</a>
                        </li>
                                              <li>
                          <a href="setka-torgovaya">Стойки для одежды</a>
                        </li>
                                              <li>
                          <a href="kronshteyny">Выставочные стенды</a>
                        </li>
                                              <li>
                          <a href="kryuchki">Доп оборудывние</a>
                        </li>
                                              <li>
                          <a href="nastennye-sistemy">Сетка хромированная</a>
                        </li>
                                              <li>
                          <a href="trosovye-sistemy">Рекламные конструкции</a>
                        </li>
                                              <li>
                          <a href="fermy-hromirovannye">Оборудывание для косметики</a>
                        </li>
                                              <li>
                          <a href="zerkala-napolnye">Эксклюзивные изделия</a>
                        </li>
                                           
                                          </ul>
                  </li>
                                                                      <li>
                      <a href="about">Проекты<span class="menu-sub__toggle"><svg><use xlink:href="http://new-alta-market.1gb.ua/wp-content/themes/alta-market-new/assets/icons/sprite_symbol.svg#icon_triangle_down"></use></svg></span></a>
                                              <ul class="menu-sub">
                                                      <li>
                              <a href="video">Видео</a>
                            </li>
                                                      <li>
                              <a href="manufacture">Производство</a>
                            </li>
                                                  </ul>
                                          </li>
                                      
                                      <li>
                      <a href="#">Клиенту<span class="menu-sub__toggle"><svg><use xlink:href="http://new-alta-market.1gb.ua/wp-content/themes/alta-market-new/assets/icons/sprite_symbol.svg#icon_triangle_down"></use></svg></span></a>
                                              <ul class="menu-sub">
                                                      <li>
                              <a href="https://www.etc.ua/%D0%9A%D0%90%D0%A2%D0%90%D0%9B%D0%9E%D0%93_ETC.pdf">Новости</a>
                            </li>
                                                      <li>
                              <a href="articles">Статьи</a>
                            </li>
                                                      <li>
                              <a href="delivery">Доставка и оплата</a>
                            </li>
                                                      <li>
                              <a href="return">Возврат</a>
                            </li>
                                                      <li>
                              <a href="warranty">Гарантии</a>
                            </li>
                                                      <li>
                              <a href="news">Новости</a>
                            </li>
                                                  </ul>
                                          </li>
                                      <li>
                      <a href="contacts">Контакты</a>
                                          </li>
                                                </ul> -->
      </div>
    </div>
    <div class="col-md-2 offset-md-1">
      <div class="header__additional">
           <!--  <a href="tel:+380442580258" class="header__additional-phone hide">+38 (044) 258-0-25 8</a>-->
            <div class="header__additional-tools">
              <div class="tools">
                <?php alta_market_new_woocommerce_cart_link();  ?>
<div class="cart__popup" id="js-cart__popup">

</div>


                                  <a href="#" class="tools__item--count js-login-toggle">
                    <svg>
                      <use xlink:href="#icon_heart"></use>
                    </svg>
                    <small class="js-wish-count">(0)</small>
                  </a>
 <?php if ( is_user_logged_in() ) { ?><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>" class="tools__item js-login-toggle tools__item--md-right">
                    <svg>
                      <use xlink:href="#icon_user"></use>
                    </svg>
                  </a>
  <?php }
 else { ?>
   <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="tools__item js-login-toggle tools__item--md-right">
                    <svg>
                      <use xlink:href="#icon_user"></use>
                    </svg>
                  </a>
  <?php } ?>    
                                <div class="tools__item d-lg-none">
                  <div class="hamburger hamburger--squeeze">
                    <div class="hamburger-box">
                      <div class="hamburger-inner"></div>
                    </div>
                  </div>
                </div>
                <div class="login__popup js-login-popup">
                  <div class="login__popup-overlay js-login-toggle"></div>
                  <h4 class="login__popup-title">Вход в личный кабинет</h4>
                  <div class="login__popup-error js-login-error js-error"></div>
                  <form class="form js-login-form" novalidate="novalidate">
                    <div class="form__item">
                      <label for="email">Email</label>
                      <input type="text" class="js-login-input" id="email_" name="email" required="required">
                    </div>
                    <div class="form__item">
                      <label for="password">Пароль</label>
                      <input type="password" class="js-login-input" id="password" name="password" required="required">
                    </div>
                    <button type="button" class="js-login-submit">Войти</button>
                    <div class="social-login">
                      <span class="social-login__title">Войти с помощью:</span>
                      <div class="social-login__list">
                        <a href="https://accounts.google.com/o/oauth2/auth?response_type=code&amp;access_type=offline&amp;client_id=571212898185-1a7eqaflcs8chlb8r549cflut541r089.apps.googleusercontent.com&amp;redirect_uri=https%3A%2F%2Fwww.etc.ua%2Findex.php%3Froute%3Daccount%2FgoogleLogin&amp;state&amp;scope=email%20profile&amp;approval_prompt=auto&amp;include_granted_scopes=true" class="social-login__link">
                          <img data-pagespeed-lazy-src="image/logo_google.svg" alt="google" data-pagespeed-url-hash="593423576" src="media/image/lion.jpg">
                        </a>
                        <button type="button" onclick="facebookLogin('main')" class="social-login__link">
                          <img data-pagespeed-lazy-src="image/logo_facebook.svg" alt="facebook" data-pagespeed-url-hash="1665111117" src="media/image/lion.jpg">
                        </button>
                      </div>
                    </div>
                  </form>
                 <!--  <div id="fb-root"></div>
                  <div class="facebook__login">
                    <form action="/index.php?route=account/facebookLogin" class="" id="facelogin" method="post" enctype="multipart/form-data" style="display: none;">
                      <input type="hidden" id="fbid" name="fbid" value="" placeholder="" class="form-control">
                      <input type="hidden" name="fbfname" value="" placeholder="" class="form-control">
                      <input type="hidden" name="fblname" value="" placeholder="" class="form-control">
                      <input type="hidden" name="fbmail" value="" placeholder="" class="form-control">

                      <div onlogin="facebookLogin('main')" class="fb-login-button focused" data-max-rows="1" data-size="medium" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>
                      <input type="hidden" id="fbin" value="<?php echo $button_login; ?>" class="btn btn-primary">
                    </form>
                  </div> -->
                  <div class="login__popup-links">
                    <a href="#" class="js-forgot-toggle">Забыли пароль?</a>
                    <a href="/index.php?route=account/register">Регистрация</a>
                  </div>
                </div>
                <div class="login__popup js-forgot-popup">
                  <div class="login__popup-overlay js-forgot-toggle"></div>
                  <h4 class="login__popup-title">Восстановление пароля</h4>
                  <div class="login__popup-error js-forgot-error js-error"></div>
                  <form class="form js-forgot-form" novalidate="novalidate">
                    <div class="form__item">
                      <label for="forgot-email">Email</label>
                      <input type="text" class="js-login-input" id="forgot-email" name="email" required="required">
                    </div>
                    <button type="button" class="js-forgot-submit">Напомнить пароль</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
  </div>
	</header><!-- #masthead -->
<?php //echo is_front_page() ? 'main' : ''; ?>
	<div id="content" class="site-content main">
