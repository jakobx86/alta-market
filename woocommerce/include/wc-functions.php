<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open',10 );

add_action('woocommerce_before_shop_loop_item','alta_loop_product_link_open',10);

function alta_loop_product_link_open()
{
	echo '<a href="'. get_permalink() .'" class="product-card__thumb">';

}
remove_action( 'woocommerce_shop_loop_item_title' );



//remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
// add_action( 'woocommerce_before_shop_loop_item_title', , $accepted_args = 1 )
add_action( 'woocommerce_shop_loop_item_title', 'alta_market_product_card_info_open', 1 );
function alta_market_product_card_info_open ()
{
	echo '<header class="product-card__footer">';
}
add_action( 'woocommerce_after_shop_loop_item','alta_market_product_card_info_close', 15 );
function alta_market_product_card_info_close()
{
echo "</header>";
}

remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10 );

add_action( 'woocommerce_shop_loop_item_title', 'alta_loop_product_title',10);

function alta_loop_product_title() { 
 $res='<div class="product-card__text">';
$res.= '<span class="product-card__label">'.get_the_title().'</span></div>';
 echo $res;
}  

add_action('woocommerce_after_shop_loop_item_title','alata_market_rait_price_open',1);
function alata_market_rait_price_open()
{
		echo '<div class="product_price_rating">';
}

remove_action ('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',5);
add_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',11);
// add_action( 'woocommerce_before_main_content', $function_to_add, $priority = 10, $accepted_args = 1 );

add_action('woocommerce_after_shop_loop_item_title','alata_market_rait_price_close',15);
function alata_market_rait_price_close()
{
		echo '</div>';
}

add_action('woocommerce_after_shop_loop_item_title','alata_market_tools_open',16);
function alata_market_tools_open()
{
		echo '<div class="tools">';
}

// add_action('woocommerce_after_shop_loop_item_title','alata_market_detail_link',17);
// function alata_market_detail_link()
// {
// 		echo '<a href="#" class="btn">
//               Подробнее
//             </a>';
// }
add_action('woocommerce_after_shop_loop_item_title','alata_market_detail_link',18);
function alata_market_detail_link()
{
		echo '<a href="'.get_permalink( $product->id ).'" class="btn">
              Подробнее
            </a>';
}

add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 19 );

add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_favorite', 20 );
function woocommerce_template_loop_add_to_favorite()
{
		echo '<a  class="tools__item js-to-login">
                <svg>
                  <use xlink:href="#icon_heart"></use>
                </svg>
              </a>';
}

add_action('woocommerce_after_shop_loop_item_title','alata_market_tools_close',25);
function alata_market_tools_close()
{
		echo '</div>';
}

/*Detail page*/

/*Wraper open*/

remove_action( 'woocommerce_before_main_content','woocommerce_output_content_wrapper',10 );

add_action( 'woocommerce_before_main_content', 'alta_output_content_wrapper',10);

function alta_output_content_wrapper() { 
 echo '<section class="category-sect">
    <div class="container single-product">';
}


/*Title*/

add_action( 'woocommerce_before_main_content', 'alta_output_detail_page_title',15);

function alta_output_detail_page_title() { 
 echo '<div class="heading--noupper">
        <h1>'.get_the_title().'</h1>
      </div>';
} 

/*Breadcrumbs*/

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb',20);
add_action( 'woocommerce_before_main_content', 'alta_breadcrumb',20);

function alta_breadcrumb() { ?>
<div class="row">
      <div class="col-12">
        <div class="breadcrumbs">
        	<?php woocommerce_breadcrumb(array(
				'delimiter'   => '&nbsp;&#47;&nbsp;',
				'wrap_before' => '<ul class="breadcrumbs__list woocommerce-breadcrumb">',
				'wrap_after'  => '</ul>',
				'before'      => '<li>',
				'after'       => '</li>',				
			)); ?>
        </div>
    </div>
</div>
<?php
}

/**/

add_action( 'woocommerce_before_single_product_summary', 'alta_product_top_open',5);




/*Single product right_column*/

function alta_product_top_open() { ?>
<div class="row">	
<?php
}
add_action( 'woocommerce_before_single_product_summary', 'alta_product_img_top_open',15);

function alta_product_img_top_open() { ?>
<div class="col-12 col-lg-5">	
<?php
}
add_action( 'woocommerce_before_single_product_summary', 'alta_product_img_top_close',25);

function alta_product_img_top_close() { ?>
</div>
 <div class="col-12 col-lg-4 offset-md-1">
<?php
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating',10);

add_action( 'woocommerce_single_product_summary', 'alta_product_info_open',9);
function alta_product_info_open()
{
?>
<div class="product__info">
            <div class="product__descr">
            	<div class="product__price-wrapper">
                

<?php
}

add_action( 'woocommerce_single_product_summary', 'alta_product_labels',12);
function alta_product_labels()
{
?>
<div class="product__labels">
                    <span class="product__labels-item">В наличии</span>
                  </div>
              </div>
<?php
}

add_action( 'woocommerce_single_product_summary', 'alta_product_info_close',21);
function alta_product_info_close()
{
?>
</div>

<?php
}
add_action( 'woocommerce_before_add_to_cart_form', 'alta_before_add_to_cart_form_open');
function alta_before_add_to_cart_form_open()
{
?>
<div class="product__tools">
<?php
}

add_action( 'woocommerce_after_add_to_cart_form', 'alta_before_add_to_cart_form_close');
function alta_before_add_to_cart_form_close()
{
?>
</div>
<?php
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta',40);

add_action( 'woocommerce_single_product_summary', 'alta_product_top_close',70);

function alta_product_top_close() { ?>
</div>
</div>

</div>
<?php
}

add_action( 'woocommerce_after_single_product_summary', 'alta_output_product_data_tabs_open',9);

function alta_output_product_data_tabs_open() { ?>
<div class=" row charecteristics_reviews_wraper woocommerce-tabs wc-tabs-wrapper">
<?php
}
add_action( 'woocommerce_after_single_product_summary', 'alta_output_product_data_tabs_close',11);

function alta_output_product_data_tabs_close() { ?>
<div class="col-xs-12 col-sm-5 w-39-9">
  <div class="reviews-product-wraper">
        <?php comments_template(); ?> 
      </div>
      </div>
</div>
<?php
}
remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar', 10 );
