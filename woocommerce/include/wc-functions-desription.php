<?php
function my_custom_physical_product_tab( $tabs ) {
	global $product;
	// Ensure it doesn't show for virtual 
	if ( isset( $tabs['description'] ) ) {
		$tabs['description']['priority'] = 10;
	}
	
	if ( isset( $tabs['additional_information'] ) ) {
		$tabs['additional_information']['priority'] = 20;
		$tabs['additional_information']['title'] = "Технические характеристики";
	}
	
	
		 unset( $tabs['reviews'] ); 	
	

	// if ( ! $product->is_virtual() ) {
	// 	$tabs['shipping'] = array(
	// 		'title'    => __( 'Описание', 'textdomain' ),
	// 		'callback' => 'my_custom_shipping_tab',
	// 		'priority' => 30,
	// 	);
	// }
//get_vd($tabs);
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'my_custom_physical_product_tab' );
/**
 * Function that displays output for the shipping tab.
 */
// function my_custom_shipping_tab( $slug, $tab ) {
// 	global $post;

// $heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) ) );

?>
