<?php 
if ( ! function_exists( 'alta_market_new_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function alta_market_new_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		alta_market_new_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'alta_market_new_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'alta_market_new_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function alta_market_new_woocommerce_cart_link() {
		?>
		<svg style="display: none;">
			<symbol version="1.1" id="icon_cart" width=26 height=26  x="0px" y="0px" viewBox="243 243 26 26"><path d="M251.4,259.8L251.4,259.8C251.5,259.8,251.5,259.8,251.4,259.8l13.6,0c0.3,0,0.6-0.2,0.7-0.5l3-10.5c0.1-0.2,0-0.5-0.1-0.7c-0.1-0.2-0.4-0.3-0.6-0.3h-18.3l-0.5-2.4c-0.1-0.3-0.4-0.6-0.7-0.6H244c-0.4,0-0.8,0.3-0.8,0.8s0.3,0.8,0.8,0.8h3.9c0.1,0.4,2.6,11.5,2.7,12.2c-0.8,0.3-1.4,1.1-1.4,2.1c0,1.2,1,2.3,2.3,2.3h13.6c0.4,0,0.8-0.3,0.8-0.8s-0.3-0.8-0.8-0.8h-13.6c-0.4,0-0.8-0.3-0.8-0.8C250.7,260.1,251,259.8,251.4,259.8zM267.1,249.3l-2.6,9h-12.4l-2-9H267.1z"/><path d="M250.7,265c0,1.2,1,2.3,2.3,2.3c1.2,0,2.3-1,2.3-2.3s-1-2.3-2.3-2.3C251.7,262.8,250.7,263.8,250.7,265z M253,264.3c0.4,0,0.8,0.3,0.8,0.8s-0.3,0.8-0.8,0.8c-0.4,0-0.8-0.3-0.8-0.8S252.5,264.3,253,264.3z"/><path d="M261.3,265c0,1.2,1,2.3,2.3,2.3s2.3-1,2.3-2.3s-1-2.3-2.3-2.3S261.3,263.8,261.3,265z M263.6,264.3c0.4,0,0.8,0.3,0.8,0.8s-0.3,0.8-0.8,0.8s-0.8-0.3-0.8-0.8S263.1,264.3,263.6,264.3z"/></symbol>
		</svg>
		<a data-src="#js-cart__popup" href="<?php echo esc_url( wc_get_cart_url() ); ?>"   class=" cart-contents tools__item--count js-header__cart">
  <svg>
    <use xlink:href="#icon_cart"></use>
  </svg>
  <small class="header__cart-count"><?php echo "(" . wp_kses_data( WC()->cart->get_cart_contents_count() ) . ")" ;?></small>
</a>
		
		<?php
	}
}

if ( ! function_exists( 'alta_market_new_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function alta_market_new_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php alta_market_new_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => 'redtdg',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}
 ?>